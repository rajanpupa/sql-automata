#!/usr/bin/perl -w
use DBI;
use Term::ReadKey;
use Sys::Hostname;
use DBI;


#   Created By  :   Rajan Prasad Upadhyay(i80991)
#   email       :   rajanprasadupadhyay@gmail.com


use File::Find;

print "Argument 0 is $ARGV[0]\n";

if( !exists $ARGV[0] ) {
    print "Missing svn location $ARGV[1] \n";
    exit;
}
else
{
	$sv = $ARGV[0];
	print "SVN Location is: ".$ARGV[1]."\n\n";
}

$schemaname = $ARGV[1];
$password   = $ARGV[2];
$thisnode   = $ARGV[3];
$vblport    = $ARGV[4];
$HHServer   = "";
if(exists $ARGV[5])
{
	$HHServer   = $ARGV[5];
}

print "matrics 1\n";
print "svn location :     $sv\n";
print "schemaname   :     $schemaname\n";
print "password     :     $password\n";
print "thisnode     :     $thisnode\n";
print "vblport      :     $vblport\n";
print "HHServer     :     $HHServer\n";

$hawkeyefront= $schemaname;
$hawkeyecode= $schemaname;
$hawkeyehist= "HH". substr $schemaname,2, 7;
$hawkeyeprescrub= "HP".substr $schemaname,2;
$hawkeyescrub= "HS".substr $schemaname,2;
$hawkeyemaster = "HAWKEYEMASTER5";
$hawkeyeqrm = "HAWKEYEQRM5";

#-------------------------------------------------------------------
$CombineObjects = "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine/SchemaScripts.sql";
$CombineEngine = "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine/CombinedEngine.sql";

print "$schemaname/$password\@$thisnode \@$CombineObjects\n";
print "This may take some time. Please check the status of execution from sql tool";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineObjects";


