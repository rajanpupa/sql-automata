#!/usr/bin/perl -w
use DBI;
use Term::ReadKey;
use Sys::Hostname;
use DBI;

#   Created By  :   Rajan Prasad Upadhyay(i80991)
#   email       :   rajanprasadupadhyay@gmail.com

use File::Find;

print "Argument 0 is $ARGV[0]\n";

if( !exists $ARGV[0] ) {
    print "Missing svn location $ARGV[1] \n";
    exit;
}
else
{
	$sv = $ARGV[0];
	print "SVN Location is: ".$ARGV[1]."\n\n";
}

$schemaname = $ARGV[1];
$password   = $ARGV[2];
$thisnode   = $ARGV[3];
$vblport    = $ARGV[4];
$HHServer   = "";
if(exists $ARGV[5])
{
	$HHServer   = $ARGV[5];
}

print "matrics 1\n";
print "svn location :     $sv\n";
print "schemaname   :     $schemaname\n";
print "password     :     $password\n";
print "thisnode     :     $thisnode\n";
print "vblport      :     $vblport\n";
print "HHServer     :     $HHServer\n";
# print "schemaname :     $schemaname\n";

print "Enter something----------------------------------\n";
##$temp = <STDIN>;

#print "Enter Password for Schame:\n";
#ReadMode('noecho');
#$password = ReadLine(O); 
#ReadMode('normal');

system "rm -rf /home/oracle/rajan/perl/Perl_RulesEngine/miniengine";
print " Directory miniengine removed.\n\n";

system "mkdir /home/oracle/rajan/perl/Perl_RulesEngine/miniengine -p";
print " Directory miniengine created.....\n\n";

system "chmod 777 /home/oracle/rajan/perl/Perl_RulesEngine/miniengine";
print "Grant provided on directory miniengine ....\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine";

system "pwd";
print "\n";

print "svn checkout $sv";

system "svn checkout $sv";
print "$PWD svn files copied..\n\n";

system "rm -rf .svn";

system "pwd";
print ".svn directory removed.\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine";
system "rm -rf .svn";

system "pwd";
print ".svn directory removed.\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine/EngineModules";
system "rm -rf .svn";
system "pwd";
print ".svn directory removed.\n\n";

system "touch 99_VH_EXIT.sql";
system 'echo "exit;" >99_VH_EXIT.sql';

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine/ObjectScripts";
system "rm -rf .svn";
system "pwd";
print ".svn directory removed.\n\n";

system "touch 99_VH_EXIT_OS.sql";
system 'echo "exit;" >99_VH_EXIT_OS.sql';

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine";

system "pwd";

########################################################### Combined Engine Script starts here #######################################################

our @newFileList;
our @oldFileList;
our @arrFileList;
our @tempList;
our $file_name;
my @raw_data;
my $myfile;
my @fileList;
my $i;
my $j;

my $line;
my $fileNumber = -1;

open (WriteFileCE, ">CombinedEngine.sql") or die $!;
open (WriteFileSC, ">SchemaScripts.sql") or die $!; 


open (WriteFile, ">esfileList.txt") or die $!; 
open (WriteFileOs, ">osfileList.txt") or die $!; 

system "pwd";

## changed by rajan
my $dir = "./";
# = shift || die "Argument missing: directory name\n";

print "Dir";
print "\n $dir \n -------------------------------------------------------------------------";
print "\n";

find(sub {print WriteFile "$File::Find::name$/" if (!/.[O|o][S|s][.]sql$/ & /.sql$/ )},$dir);
find(sub {print WriteFileOs "$File::Find::name$/" if (/.[O|o][S|s][.]sql$/ )},$dir);

close(WriteFile);
close(WriteFileOs);

################################ Combining Engine Modules ###################################
print "Hello world";
print "\nCombining Engine Modules.................\n";
sorting('esfileList.txt');

foreach $file_name (@newFileList)
{
	if($file_name !~ m/(CombinedEngine.sql)/i and $file_name !~ m/(SchemaScripts.sql)/i)
	{
		open(DATA, $file_name) or die("Error: cannot open file $file_name\n");
		@raw_data=<DATA>;
		foreach $line (@raw_data)
		{
			print WriteFileCE $line;
		}
	}
	print WriteFileCE "\n\n";
}
close(WriteFileCE);
################################Combining Object Modules ###################################

print "\n\nCombining Object Modules.................\n";

sorting('osfileList.txt');

foreach $file_name (@newFileList)
{
	if($file_name !~ m/(CombinedEngine.sql)/i and $file_name !~ m/(SchemaScripts.sql)/i)
	{
		open(DATA, $file_name) or die("Error: cannot open file $file_name\n");
		@raw_data=<DATA>;
		foreach $line (@raw_data)
		{
			print WriteFileSC $line;
		}
	}
	print WriteFileSC "\n\n";
}
close(WriteFileSC);

print "\n\nCombining Scripts sucessfully.................\n\n\n";

################################Sorting Modules ###################################

sub sorting
{
	@newFileList = 0;
	@oldFileList = 0;
	@arrFileList = 0;
	@tempList = 0;
	$file_name = 0;
	@raw_data = 0;
	$myfile = 0;
	@fileList = 0;
	$i = 0;
	$j =0;
	$fileNumber = -1;

	my $file_nm = $_[0];
	
	$fileNumber = -1;
	open(FileName, $file_nm) or die("Error: cannot open file 'file.txt'\n");
	while( $file_name = <FileName> )
	{
		$fileNumber = $fileNumber +1;
		chomp($file_name);
		$oldFileList[$fileNumber] = $file_name;
		@arrFileList=split(/\//,$file_name);
		foreach $myfile (@arrFileList)
		{
			if($myfile =~m/(.sql)/i)
			{
				$fileList[$fileNumber] = $myfile;
			}
		}
	}

	@tempList = sort(@fileList);
	for $i (0 .. $#tempList)
	{
		foreach $j (@oldFileList)
		{
			if($j =~ m/($tempList[$i])/)
			{
				$newFileList[$i] = $j;
			}
		}
	}
	close(FileName);
}

################################################ End of Combine Engine #############################################################

system "rm /home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine/*.txt";

$CombineObjects = "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine/SchemaScripts.sql";
$CombineEngine = "/home/oracle/rajan/perl/Perl_RulesEngine/miniengine/000_0_MiniEngine/CombinedEngine.sql";

system "sqlplus $schemaname/$password\@$thisnode \@$CombineObjects";

print "system \"sqlplus $schemaname/$password\@$thisnode \@$CombineEngine\"  ";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineEngine";

print "-----------------sqlplus $schemaname/$password\@$thisnode \@$CombineObjects\n";
print "-----------------Compilation completed, now exiting the script rajan";

exit;




#*****************************************************************
$db1 = DBI->connect( "dbi:Oracle:$thisnode", $schemaname, $password) || die( $DBI::errstr . "\n" );
$db1->{AutoCommit}    = 0;
$db1->{RaiseError}    = 1;
$db1->{ora_check_sql} = 0;
$db1->{RowCacheSize}  = 16;

$sql1 = "begin SP_ProcessMiniengine(4,'$HHServer',$vblport); end;";
$sth1 = $db1->prepare($sql1);
$sth1->execute();
$db1->disconnect if defined($db1);
print "Engine executed successfully..... \n\n";

#*****************************************************************
$db1 = DBI->connect( "dbi:Oracle:$thisnode", $schemaname, $password) || die( $DBI::errstr . "\n" );
$db1->{AutoCommit}    = 0;
$db1->{RaiseError}    = 1;
$db1->{ora_check_sql} = 0;
$db1->{RowCacheSize}  = 16;
$sql1 = "begin SP_Mini_HHDBL('$HHServer','DROP'); end;";
$sth1 = $db1->prepare($sql1);
$sth1->execute();
#*****************************************************************
print "DBLink Dropped Successfully..... \n\n";
END {
    $db1->disconnect if defined($db1);
}
exit;
#*****************************************************************