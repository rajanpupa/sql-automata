# MAKE SHELL EXECUTABLE
# Creator : Rajan Prasad Upadhyay
# NOTE:     This file is in development mode.
#           Use at your own risk.
#           Any kind of crash or so, is not the responsibility of the creator.
SHELL=/bin/sh

ifndef time
	time = 1m
endif

# MRE svn url
ifndef svn_url_mre
	#svn_url_mre  = https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Trunk/BackEnd/DBScripts/Engine/000_0_MiniEngine/
	#svn_url_mre = https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Branches/BackEnd/HRS_UAT/Engine/000_0_MiniEngine/
	svn_url_mre = https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Branches/BackEnd/6.3.0/Engine/000_0_MiniEngine/
endif

# MI svn repository
ifndef svn_url_mi
	#svn_url_mi = https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Trunk/BackEnd/DBScripts/Engine/
    #svn_url_mi = https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Branches/BackEnd/HRS_UAT/Engine/
    svn_url_mi=https://svn.datacenter.d2hawkeye.net/svn/d2svn/VHProducts/SMI/MI/Branches/BackEnd/6.3.0/Engine/
endif

# Schema mre
ifndef SCHEMA
	mre_schema = VC0974001140722
endif
# Schema mi
ifndef SCHEMA
	mi_schema = HF0974003140723
endif

# password
ifndef password
	password = oracle
endif


ifndef sid
	sid = d2he
endif

ifndef port
	port = 1521
endif

ifndef server
	server = nvscmdbq1
endif

#./miniengine.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server}

#The next line should begin with tab
sleep:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	./rajan_scripts/scripts/sleep.sh ${time} ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
clean_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "Cleaning the folders inside checkouts/miniengine\n";
	@rm -rf ./checkouts/miniengine/*
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
drop_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "Dropping non scrub objects from ${mre_schema} ";
	./executeSqlFile.pl ${mre_schema} ${password} ${sid} ./rajan_scripts/drop_non_scrub_objects_mre.sql ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
checkout_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "checkout_mre.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server}";
	./mre_checkout.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server} ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
compile_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "miniengine.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server}";
	./mre_compile.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server} ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
run_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "miniengine.pl ${svn_url_mre} ${mre_schema} ${password} ${sid} ${port} ${server}";
	./executeSqlFile.pl ${mre_schema} ${password} ${sid} ./rajan_scripts/mre_run.sql ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
resume_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "resume command under construction";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
schedule_mre:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "schedule_mre under construction";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
#
#
#
clean_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "Cleaning the folders inside checkouts/miengine\n";
	@rm -rf ./checkouts/miengine/*
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
drop_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "executeSqlFile.pl ${mi_schema} ${password} ${sid} ./rajan_scripts/drop_non_scrub_objects_mi.sql ";
	./executeSqlFile.pl ${mi_schema} ${password} ${sid} ./rajan_scripts/drop_non_scrub_objects_mi.sql ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
checkout_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "mi_checkout.pl ${svn_url_mi} ${mre_schema} ${password} ${sid} ${port} ${server}";
	./mi_checkout.pl ${svn_url_mi} ${mi_schema} ${password} ${sid} ${port} ${server} ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
compile_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "MI non scrub object COMPILE modules, UNDER CONSTRUCTION. only checks out";
	./mi_compile.pl ${svn_url_mi} ${mi_schema} ${password} ${sid} ${port} ${server} ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
run_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "    Run command Under construction " ;
	./executeSqlFile.pl ${mi_schema} ${password} ${sid} ./rajan_scripts/mi_run.sql ;
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
resume_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	./printMessage.sh "Resume command Under construction ";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
schedule_mi:
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	./printMessage.sh "Schedule command Under construction ";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
help:
	@echo "----------------Help for make command------------------";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
	@echo "List of Valid parameters:svn_url_mre=*hrs_uat*, mre_schema=VC0974001140722, password=oracle, sid=d2he, port=1521, server=nvscmdbq1 ";
	@echo "drop_mre_objects : drops the mre";
	@echo "compile_mre : compiles the mre";
	@echo "run : under construction";
	@echo "resume : under construction";
	@echo "schedule : under construction";
	@echo "help : Prints this help :)";
	@echo "Implemented by Rajan Prasad Upadhyay(i80991) \n";
