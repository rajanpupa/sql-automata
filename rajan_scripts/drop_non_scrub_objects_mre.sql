/*
 *  Created by  :   Rajan Prasad Upadhyay
 *  Date        :   27 August 2014
 *  
 *  Note        :
 *  Drops All the stored procedures, packages, functions, triggers, types, views, sequences etc.
 *  Drops tables like VH_DX_%, but not (VH_%, %_BK%, HR_%)
 */
DECLARE
    VB_DROP_QUERY   VARCHAR2(4000);
BEGIN
    IF(SUBSTR(USER, 1, 2)) != 'VC'
    THEN 
        DBMS_OUTPUT.PUT_LINE('NOT A VC* SCHEMA!!!');
        RETURN;
    END IF;
    
    FOR VB_DROP_SQL IN 
          (
              SELECT drop_objects from (
				      SELECT 
						      CASE object_type 
                  WHEN 'TABLE' THEN 'DROP table '||object_name||' CASCADE CONSTRAINTS;'
						      WHEN 'VIEW' THEN 'DROP VIEW '||object_name||';'
						      WHEN 'SEQUENCE' THEN 'DROP SEQUENCE '||object_name||';' 
						      WHEN 'SYNONYM' THEN 'DROP SYNONYM '||object_name||';'
						      WHEN 'FUNCTION' THEN 'DROP FUNCTION '||object_name||';'
						      WHEN 'PROCEDURE' THEN 'DROP PROCEDURE '||object_name||';'
						      WHEN 'PACKAGE' THEN 'DROP PACKAGE '||object_name||';'
						      WHEN 'PACKAGE BODY' THEN 'DROP PACKAGE BODY '||object_name||';'
						      WHEN 'TYPE' THEN 'DROP TYPE '||object_name||';'
						      WHEN 'TYPE BODY' THEN 'DROP TYPE BODY '||object_name||';'
						      WHEN 'MATERIALIZED VIEW' THEN 'DROP MATERIALIZED VIEW '||object_name||';' 
						      END drop_objects
				      FROM USER_objects WHERE 1=1
				      AND (object_name NOT LIKE 'VH_%' OR object_name LIKE 'VH_DX_%' )
				      AND object_name NOT LIKE 'HR_%'
				      AND OBJECT_NAME NOT LIKE '%_BK%'
                      AND OBJECT_NAME NOT LIKE '%QA_%'
			      )
			      where drop_objects is not null
        )
    LOOP
        BEGIN
             VB_DROP_QUERY := SubStr(VB_DROP_SQL.DROP_OBJECTS,1, InStr(VB_DROP_SQL.DROP_OBJECTS,';')-1);
             Dbms_Output.Put_Line(VB_DROP_QUERY);
             EXECUTE IMMEDIATE VB_DROP_QUERY;
        EXCEPTION WHEN OTHERS THEN NULL;
        END;
    
    END LOOP;

END;
/

exit;
