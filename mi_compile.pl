#!/usr/bin/perl -w
use DBI;
use Term::ReadKey;
use Sys::Hostname;
use DBI;

#   Created By  :   Rajan Prasad Upadhyay(i80991)
#   email       :   rajanprasadupadhyay@gmail.com

use File::Find;

print "Argument 0 is $ARGV[0]\n";

if( !exists $ARGV[0] ) {
    print "Missing svn location $ARGV[1] \n";
    exit;
}
else
{
	$sv = $ARGV[0];
	print "SVN Location is: ".$ARGV[1]."\n\n";
}

$schemaname = $ARGV[1];
$password   = $ARGV[2];
$thisnode   = $ARGV[3];
$vblport    = $ARGV[4];
$HHServer   = "";
if(exists $ARGV[5])
{
	$HHServer   = $ARGV[5];
}

print "matrics 1\n";
print "svn location :     $sv\n";
print "schemaname   :     $schemaname\n";
print "password     :     $password\n";
print "thisnode     :     $thisnode\n";
print "vblport      :     $vblport\n";
print "HHServer     :     $HHServer\n";
# print "schemaname :     $schemaname\n";

$hawkeyefront= $schemaname;
$hawkeyecode= $schemaname;
$hawkeyehist= "HH". substr $schemaname,2, 7;
$hawkeyeprescrub= "HP".substr $schemaname,2;
$hawkeyescrub= "HS".substr $schemaname,2;
$hawkeyemaster = "HAWKEYEMASTER5";
$hawkeyeqrm = "HAWKEYEQRM5";

############################## compilation work starts here###############################
$CombineObjects = "/home/oracle/rajan/perl/Perl_RulesEngne/checkouts/miengne/SchemaScripts.sql";
$CombineEngne = "/home/oracle/rajan/perl/Perl_RulesEngne/checkouts/miengne/CombinedEngne.sql";
$CombineObjects_custom = "/home/oracle/rajan/perl/Perl_RulesEngne/checkouts/miengne/SchemaScripts_custom.sql";
$CombineEngne_custom = "/home/oracle/rajan/perl/Perl_RulesEngne/checkouts/miengne/CombinedEngne_custom.sql";

chdir "/home/oracle/rajan/perl/Perl_RulesEngne";

system "rm -rf ./checkouts/miengne/*custom.sql";

system "cp  ${CombineObjects} ${CombineObjects_custom}";
system "cp  ${CombineEngne} ${CombineEngne_custom}  ";

print "hawkeyefront     : $hawkeyefront \n";
print "hawkeyecode      : $hawkeyecode \n";
print "hawkeyehist      : $hawkeyehist \n";
print "hawkeyeprescrub  : $hawkeyeprescrub \n";
print "hawkeyescrub     : $hawkeyescrub \n";
print "hawkeyemaster    : $hawkeyemaster \n";
print "hawkeyeqrm       : $hawkeyeqrm \n";

print "Replacing the variables Hawkeyefront, Hawkeyecode, hawkeyemaster, hawkeyeqrm, hawkeyehist, hawkeyescrub, hawkeyeprescrub\n";

#REPLACEMENTS or schemascripts.sql
system"sed -i s/\"HAWKEYEFRONT\"/$hawkeyefront/gi             ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYECODE\"/$hawkeyecode/gi               ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYEMASTER\"/$hawkeyemaster/gi           ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYEQRM\"/$hawkeyeqrm/gi                 ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYEHIST\"/$hawkeyehist/gi               ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYESCRUB\"/$hawkeyescrub/gi             ./checkouts/miengne/SchemaScripts_custom.sql ";
system"sed -i s/\"HAWKEYEPRESCRUB\"/$hawkeyeprescrub/gi       ./checkouts/miengne/SchemaScripts_custom.sql ";
#system"sed -i s/\"USER\"/$hawkeyefront/gi       ./checkouts/miengne/SchemaScripts_custom.sql ";

#REPLACEMENTS or CombinedEngne.sql
system"sed -i s/\"HAWKEYEFRONT\"/$hawkeyefront/gi             ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYECODE\"/$hawkeyecode/gi               ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYEMASTER\"/$hawkeyemaster/gi           ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYEQRM\"/$hawkeyeqrm/gi                 ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYEHIST\"/$hawkeyehist/gi               ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYESCRUB\"/$hawkeyescrub/gi             ./checkouts/miengne/CombinedEngne_custom.sql";
system"sed -i s/\"HAWKEYEPRESCRUB\"/$hawkeyeprescrub/gi       ./checkouts/miengne/CombinedEngne_custom.sql";
#system"sed -i s/\"USER\"/$hawkeyefront/gi                     ./checkouts/miengne/CombinedEngne_custom.sql";

print "Replaced the variables and exited without compile\n";
#exit;

print "Compiling the scripts.....................\n ";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineObjects_custom";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineEngne_custom";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineObjects_custom";
system "sqlplus $schemaname/$password\@$thisnode \@$CombineEngne_custom";

print "Engne compilation completed.....................\n\n";
exit;

