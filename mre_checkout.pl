#!/usr/bin/perl -w
use DBI;
use Term::ReadKey;
use Sys::Hostname;
use DBI;
#*****************************************************************
#    Run for checkouts/miniengine 
#*****************************************************************
# argument names

# use strict;
use File::Find;

print "Argument 0 is $ARGV[0]\n";

if( !exists $ARGV[0] ) {
    print "Missing svn location $ARGV[1] \n";
    exit;
}
else
{
	$sv = $ARGV[0];
	print "SVN Location is: ".$ARGV[1]."\n\n";
}

$schemaname = $ARGV[1];
$password   = $ARGV[2];
$thisnode   = $ARGV[3];
$vblport    = $ARGV[4];
$HHServer   = "";
if(exists $ARGV[5])
{
	$HHServer   = $ARGV[5];
}

print "matrics 1\n";
print "svn location :     $sv\n";
print "schemaname   :     $schemaname\n";
print "password     :     $password\n";
print "thisnode     :     $thisnode\n";
print "vblport      :     $vblport\n";
print "HHServer     :     $HHServer\n";
# print "schemaname :     $schemaname\n";

print "Enter something----------------------------------\n";
##$temp = <STDIN>;

#print "Enter Password for Schame:\n";
#ReadMode('noecho');
#$password = ReadLine(O); 
#ReadMode('normal');

system "rm -rf /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine";
print " Directory checkouts/miniengine removed.\n\n";

system "mkdir /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine -p";
print " Directory checkouts/miniengine created.....\n\n";

system "chmod 777 /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine";
print "Grant provided on directory checkouts/miniengine ....\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine";

system "pwd";
print "\n";

print "svn checkout $sv";

system "svn checkout $sv";
print "$PWD svn files copied..\n\n";

system "rm -rf .svn";

system "pwd";
print ".svn directory removed.\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine";
system "rm -rf .svn";

system "pwd";
print ".svn directory removed.\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine/EngineModules";
system "rm -rf .svn";
system "pwd";
print ".svn directory removed.\n\n";

system "touch 99_VH_EXIT.sql";
system 'echo "exit;" >99_VH_EXIT.sql';

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine/ObjectScripts";
system "rm -rf .svn";
system "pwd";
print ".svn directory removed.\n\n";

system "touch 99_VH_EXIT_OS.sql";
system 'echo "exit;" >99_VH_EXIT_OS.sql';

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miniengine/000_0_MiniEngine";

system "pwd";
exit;
