#!/usr/bin/perl -w
use DBI;
use Term::ReadKey;
use Sys::Hostname;
use DBI;

#   Created By  :   Rajan Prasad Upadhyay(i80991)
#   email       :   rajanprasadupadhyay@gmail.com


use File::Find;

print "Argument 0 is $ARGV[0]\n";

if( !exists $ARGV[0] ) {
    print "Missing svn location $ARGV[1] \n";
    exit;
}
else
{
	$sv = $ARGV[0];
	print "SVN Location is: ".$ARGV[1]."\n\n";
}

$schemaname = $ARGV[1];
$password   = $ARGV[2];
$thisnode   = $ARGV[3];
$vblport    = $ARGV[4];
$HHServer   = "";
if(exists $ARGV[5])
{
	$HHServer   = $ARGV[5];
}

print "matrics 1\n";
print "svn location :     $sv\n";
print "schemaname   :     $schemaname\n";
print "password     :     $password\n";
print "thisnode     :     $thisnode\n";
print "vblport      :     $vblport\n";
print "HHServer     :     $HHServer\n";
# print "schemaname :     $schemaname\n";

$hawkeyefront= $schemaname;
$hawkeyecode= $schemaname;
$hawkeyehist= "HH". substr $schemaname,2, 7;
$hawkeyeprescrub= "HP".substr $schemaname,2;
$hawkeyescrub= "HS".substr $schemaname,2;
$hawkeyemaster = "HAWKEYEMASTER5";
$hawkeyeqrm = "HAWKEYEQRM5";

#exit;
print "Enter something----------------------------------\n";
##$temp = <STDIN>;

#print "Enter Password for Schame:\n";
#ReadMode('noecho');
#$password = ReadLine(O); 
#ReadMode('normal');

# Delete the .svn directory from each directory
system "rm -rf /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine";
print " Directory checkouts/miengine removed.\n\n";

system "mkdir /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine -p";
print " Directory checkouts/miengine created.....\n\n";

system "chmod 777 /home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine";
print "Grant provided on directory checkouts/miengine ....\n\n";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine";

system "pwd";
print "\n";

print "svn checkout $sv";

system "svn checkout $sv";
print "$PWD svn files copied $sv\n\n";

# Remove all the hidden .svn folders from the codebase
chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine";

system "rm -fR `find |grep svn\$` ";

chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine/Engine";
system "touch 99_VH_EXIT.sql";
system 'echo "exit;" >99_VH_EXIT.sql';#exit;

system "touch 99_VH_EXIT_OS.sql";
system 'echo "exit;" >99_VH_EXIT_OS.sql';
chdir "/home/oracle/rajan/perl/Perl_RulesEngine/checkouts/miengine";

### here
########################################################### Combined Engine Script starts here #######################################################

our @newFileList;
our @oldFileList;
our @arrFileList;
our @tempList;
our $file_name;
our $actual_file;
my @raw_data;
my @raw_data2;
my $myfile;
my @fileList;
my $i;
my $j;

my $line;
my $fileNumber = -1;

# Define file pointers in write mode, for combined script
open (WriteFileCE, ">CombinedEngine.sql") or die $!;
open (WriteFileSC, ">SchemaScripts.sql") or die $!; 

# Define file pointers in write mode, for collecting files list
open (WriteFile, ">esfileList.txt") or die $!; 
open (WriteFileOs, ">osfileList.txt") or die $!; 

system "pwd";

## changed by rajan
my $dir = "./";
# = shift || die "Argument missing: directory name\n";

print "Dir";
print "\n $dir \n -------------------------------------------------------------------------";
print "\n";

find(sub {print WriteFile "$File::Find::name$/" if (!/.[O|o][S|s][.]sql$/ & /.sql$/ )},$dir);
find(sub {print WriteFileOs "$File::Find::name$/" if (/.[O|o][S|s][.]sql$/ )},$dir);

close(WriteFile);
close(WriteFileOs);

################################ Combining Engine Modules ###################################
print "Hello world";

system "cat esfileList.txt| sort > temp";
system "cat temp| sort  > esfileList.txt";

system "cat osfileList.txt| sort  > temp";
system "cat temp| sort > osfileList.txt";
#system "rm -f temp";
$file_name='esfileList.txt';

open(DATA, $file_name) or die("Error: cannot open file $file_name\n");
@raw_data=<DATA>;
foreach $actual_file (@raw_data)
{
    #print WriteFileCE $line;
    if($actual_file !~ m/(CombinedEngine.sql)/i and $actual_file !~ m/(SchemaScripts.sql)/i)
	{
        print $actual_file."\n";
		open(DATA1, $actual_file) or die("Error: cannot open file $actual_file\n");
		@raw_data2=<DATA1>;
		foreach $line (@raw_data2)
		{
			print WriteFileCE $line;
		}
	}
	print WriteFileCE "\n\n";
}

print WriteFileCE "\n\n";

close(WriteFileCE);
#-----------------------------------
$file_name='osfileList.txt';

open(DATA, $file_name) or die("Error: cannot open file $file_name\n");
@raw_data=<DATA>;
foreach $actual_file (@raw_data)
{
    #print WriteFileCE $line;
    if($actual_file !~ m/(CombinedEngine.sql)/i and $actual_file !~ m/(SchemaScripts.sql)/i)
	{
        print $actual_file."\n";
		open(DATA1, $actual_file) or die("Error: cannot open file $actual_file\n");
		@raw_data2=<DATA1>;
		foreach $line (@raw_data2)
		{
			print WriteFileSC $line;
		}
	}
	print WriteFileSC "\n\n";
}

print WriteFileSC "\n\n";

close(WriteFileSC);

exit;